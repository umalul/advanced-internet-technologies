import { Posts } from '../interfaces/posts';
import { Observable } from 'rxjs';
import { PostsService} from '../posts.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  title:string;
  body:string;
  id:number;
  userId:number;
  Posts$:Observable<Posts>;
  constructor(private PostsService: PostsService) { }

  ngOnInit(): void {
    // this.blogService.getBlog().subscribe(data => this.posts = data);
    this.Posts$ = this.PostsService.getPost();
    

  }

}
