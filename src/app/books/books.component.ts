import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Book } from '../interfaces/book';

@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})

export class BooksComponent implements OnInit {

  // books;
  books$;
  books:Book[];
  userId:string;
  editstate = [];
  editCreate:boolean = false;
  lastDocumentArrived;
  firstDocumentArrived;
  panelOpenState = false;


  
  constructor(private booksService:BooksService, public auth:AuthService) { }

  deleteBook(id:string){
    this.booksService.deleteBook(this.userId,id);
  }
  update(book:Book){
    this.booksService.updateBook(this.userId,book.id,book.title,book.author);
  }
  create(book:Book){
    this.booksService.addNewBook(this.userId,book.title,book.author);
  }

  nextPage(){
      this.books$ = this.booksService.getBooks(this.userId,this.lastDocumentArrived);
      this.books$.subscribe(
        docs => {
          this.lastDocumentArrived = docs[docs.length-1].payload.doc;
          this.firstDocumentArrived = docs[0].payload.doc;
          this.books = [];
          for(let document of docs){
            const book:Book = document.payload.doc.data();
            book.id = document.payload.doc.id;
            this.books.push(book);
          }
        }
      )
  }

  previousPage(){
    this.books$ = this.booksService.getBooks2(this.userId,this.firstDocumentArrived);
    this.books$.subscribe(
      docs => {
        this.firstDocumentArrived = docs[0].payload.doc;
        this.lastDocumentArrived = docs[docs.length-1].payload.doc;
        this.books = [];
        for(let document of docs){
          const book:Book = document.payload.doc.data();
          book.id = document.payload.doc.id;
          this.books.push(book);
        }
      }
    )
}


  ngOnInit(): void {
    this.auth.getUser().subscribe(
      user => {
      this.userId = user.uid;
      this.books$ = this.booksService.getBooks(this.userId,null);
      this.books = [];
      this.books$.subscribe(
        docs => {
          this.lastDocumentArrived = docs[docs.length-1].payload.doc;
          this.firstDocumentArrived = docs[0].payload.doc;
          this.books = [];
          for(let document of docs){
            const book:Book = document.payload.doc.data();
            book.id = document.payload.doc.id;
            this.books.push(book);
          }
        }
      )
    }
    )
  }

}
