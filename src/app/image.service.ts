import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
private path:string = 'https://firebasestorage.googleapis.com/v0/b/hello-jce-uri.appspot.com/o/';
public images:string[] = [];
  constructor() { 
    this.images[0] = this.path + 'biz.JPG?' + '?alt=media&token=eff459d7-0ddc-4337-8591-15d2857eada6';
    this.images[1] = this.path + 'entermnt.JPG' + '?alt=media&token=febc0006-2bee-4d67-8d1c-187caa1bf6dd';
    this.images[2] = this.path + 'politics-icon.png' + '?alt=media&token=d84e9c45-357b-4b63-8102-e33614ac6c49';
    this.images[3] = this.path + 'sport.JPG' + '?alt=media&token=4b9752b9-00a7-42a2-85c8-0da34646cda6';
    this.images[4] = this.path + 'tech.JPG' + '?alt=media&token=f8f9a7ce-4a2e-43e1-96a1-f52c43d86b29';
  }
}
