import { WeatherRaw } from './interfaces/weather-raw';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Weather } from './interfaces/weather';
import { throwError, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "8c79fbe9750440431d9bb14836dc4f7c";
  private IMP = "units=metric";

  constructor(private Http:HttpClient ) { }

  searchWeatherData(cityName:string):Observable<Weather>{
    return this.Http.get<WeatherRaw>(`${this.URL}${cityName}&appid=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transformWheatherData(data)),
      catchError(this.handleError)
    )
  }

  private handleError(res:HttpErrorResponse){
    console.log(res.error)
    return throwError(res.error || 'Server Error');
  }

  private transformWheatherData(data:WeatherRaw):Weather{
    return{
      name:data.name,
      country:data.sys.country,
      image:`https://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description:data.weather[0].description,
      temperature:data.main.temp,
      lat:data.coord.lat,
      lon:data.coord.lon
    }
}


}
