import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';



@Injectable({
  providedIn: 'root'
})
export class BooksService {
  
  books = [{title:'Alice in Wonderland', author:'Lewis Carrol', summary:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."},
          {title:'War and Peace', author:'Leo Tolstoy', summary:"Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words"},
          {title:'The Magic Mountain', author:'Thomas Mann', summary:"There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text."}];
  public addBooks(){
    setInterval(() => this.books.push({title:'A new one',author:'New Author',summary:'Short Summary'}),2000);
  }
          
  bookCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');


  public getBooks(userId,startAfter){
    this.bookCollection = this.db.collection(`users/${userId}/books`,
    ref => ref.orderBy('title','asc').limit(4).startAfter(startAfter));
    return this.bookCollection.snapshotChanges()
  }

  public getBooks2(userId,endBefore){
    this.bookCollection = this.db.collection(`users/${userId}/books`,
    ref => ref.orderBy('title','asc').limitToLast(4).endBefore(endBefore));
    return this.bookCollection.snapshotChanges()
  }

  public deleteBook(Userid:string,id:string){
    this.db.doc(`users/${Userid}/books/${id}`).delete();
  }

  public updateBook(userId:string,id:string,title:string,author:string){
    this.db.doc(`users/${userId}/books/${id}`).update(
      {
        title:title,
        author:author
      }
    )
  }

  public addNewBook(userId:string,title:string,author:string){
    const book = {title:title, author:author};
    this.userCollection.doc(userId).collection('books').add(book);
  }
  
  constructor(private db:AngularFirestore) { }





  
}
