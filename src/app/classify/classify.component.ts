import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ImageService } from '../image.service';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {
    favoriteChannel: string;
    channels: string[] = ['BBC', 'CNN', 'NBC'];
    text: string;
    category: string = 'No Category';
    categoryImage:string;
    classify(){
      this.classifyService.classify(this.text).subscribe(
        res => {
          console.log(res);
          this.category = this.classifyService.categories[res];
          this.categoryImage = this.imageService.images[res];
        }
      )
    }

  constructor(private route:ActivatedRoute,private classifyService:ClassifyService,private imageService:ImageService) { }

  ngOnInit(): void {
  
    this.favoriteChannel = this.route.snapshot.params.channels
    
  }
}
