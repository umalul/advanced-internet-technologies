import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Posts } from './interfaces/posts';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private URL :string = "https://jsonplaceholder.typicode.com/posts/";
  
  constructor(private http:HttpClient) { }

  getPost():Observable<Posts>{
    return this.http.get<Posts>(`${this.URL}`);
  }

  // private transformPostData(data:Posts):Posts{
  //   return{
  //    id:data.id,
  //    title:data.title,
  //    body:data.body,
  //    userId:data.userId

  //   }
  // }

  

  // private handleError(res:HttpErrorResponse){
  //   console.log(res.error);
  //   return throwError(res.error || 'Server Error')
 
  

}
