// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyB1MIOs0s4jw01IYWG5xa3L0SBVH5jn1R8",
    authDomain: "hello-jce-uri.firebaseapp.com",
    databaseURL: "https://hello-jce-uri.firebaseio.com",
    projectId: "hello-jce-uri",
    storageBucket: "hello-jce-uri.appspot.com",
    messagingSenderId: "742463096195",
    appId: "1:742463096195:web:ef1d4e693e23b0b249a2b6"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
